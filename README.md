## Setting up Numlocks on a Debian Stretch with LightDM

Setting up Numlocks may be useful on Debian if the numeric keypad is incorrectly recognised. Enabling Numlockx will also enable the numeric keypad at startup. Here's how to do it :

### 1. Installing Numlockx

```
$ sudo apt install numlockx
```

### 2. Enabling Numlockx

Very easy, just add a line at the end of the file `/usr/share/lightdm/lightdm.conf.d/01_debian.conf`:

```
$ cd /usr/share/lightdm/lightdm.conf.d
$ sudo cp 01_debian.conf 01_debian.conf.bak
$ sudo su
# echo "greeter-setup-script=/usr/bin/numlockx on" >> 01_debian.conf 
```

The method may vary depending on your display manager.

==================================

## Configurer Numlocks sur une Debian Stretch avec LightDM

Setting up Numlocks may be useful on Debian if the numeric keypad is incorrectly recognised. Enabling Numlockx will also enable the numeric keypas at startup. Here's how to do it :

### 1. Installer Numlockx

```
$ sudo apt install numlockx
```

### 2. Activer Numlockx

C'est très simple, il suffit d'ajouter une ligne à la fin du fichier `/usr/share/lightdm/lightdm.conf.d/01_debian.conf` :

```
$ cd /usr/share/lightdm/lightdm.conf.d
$ sudo cp 01_debian.conf 01_debian.conf.bak
$ sudo su
# echo "greeter-setup-script=/usr/bin/numlockx on" >> 01_debian.conf 
```

La méthode peut varier en fonction de votre gestionnaire d'affichage.